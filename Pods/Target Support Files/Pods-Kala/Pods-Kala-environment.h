
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Alamofire
#define COCOAPODS_POD_AVAILABLE_Alamofire
#define COCOAPODS_VERSION_MAJOR_Alamofire 1
#define COCOAPODS_VERSION_MINOR_Alamofire 3
#define COCOAPODS_VERSION_PATCH_Alamofire 0

// Bond
#define COCOAPODS_POD_AVAILABLE_Bond
#define COCOAPODS_VERSION_MAJOR_Bond 3
#define COCOAPODS_VERSION_MINOR_Bond 10
#define COCOAPODS_VERSION_PATCH_Bond 0

// BubbleTransition
#define COCOAPODS_POD_AVAILABLE_BubbleTransition
#define COCOAPODS_VERSION_MAJOR_BubbleTransition 0
#define COCOAPODS_VERSION_MINOR_BubbleTransition 2
#define COCOAPODS_VERSION_PATCH_BubbleTransition 0

// CBStoreHouseRefreshControl
#define COCOAPODS_POD_AVAILABLE_CBStoreHouseRefreshControl
#define COCOAPODS_VERSION_MAJOR_CBStoreHouseRefreshControl 1
#define COCOAPODS_VERSION_MINOR_CBStoreHouseRefreshControl 0
#define COCOAPODS_VERSION_PATCH_CBStoreHouseRefreshControl 0

// CarbonKit
#define COCOAPODS_POD_AVAILABLE_CarbonKit
#define COCOAPODS_VERSION_MAJOR_CarbonKit 1
#define COCOAPODS_VERSION_MINOR_CarbonKit 1
#define COCOAPODS_VERSION_PATCH_CarbonKit 2

// DOFavoriteButton
#define COCOAPODS_POD_AVAILABLE_DOFavoriteButton
#define COCOAPODS_VERSION_MAJOR_DOFavoriteButton 0
#define COCOAPODS_VERSION_MINOR_DOFavoriteButton 0
#define COCOAPODS_VERSION_PATCH_DOFavoriteButton 4

// HanekeSwift
#define COCOAPODS_POD_AVAILABLE_HanekeSwift
#define COCOAPODS_VERSION_MAJOR_HanekeSwift 0
#define COCOAPODS_VERSION_MINOR_HanekeSwift 9
#define COCOAPODS_VERSION_PATCH_HanekeSwift 1

// KTSecretTextView
#define COCOAPODS_POD_AVAILABLE_KTSecretTextView
#define COCOAPODS_VERSION_MAJOR_KTSecretTextView 0
#define COCOAPODS_VERSION_MINOR_KTSecretTextView 0
#define COCOAPODS_VERSION_PATCH_KTSecretTextView 1

// LGPlusButtonsView
#define COCOAPODS_POD_AVAILABLE_LGPlusButtonsView
#define COCOAPODS_VERSION_MAJOR_LGPlusButtonsView 1
#define COCOAPODS_VERSION_MINOR_LGPlusButtonsView 0
#define COCOAPODS_VERSION_PATCH_LGPlusButtonsView 3

// PullToMakeFlight
#define COCOAPODS_POD_AVAILABLE_PullToMakeFlight
#define COCOAPODS_VERSION_MAJOR_PullToMakeFlight 1
#define COCOAPODS_VERSION_MINOR_PullToMakeFlight 0
#define COCOAPODS_VERSION_PATCH_PullToMakeFlight 0

// RQShineLabel
#define COCOAPODS_POD_AVAILABLE_RQShineLabel
#define COCOAPODS_VERSION_MAJOR_RQShineLabel 0
#define COCOAPODS_VERSION_MINOR_RQShineLabel 1
#define COCOAPODS_VERSION_PATCH_RQShineLabel 3

// SABlurImageView
#define COCOAPODS_POD_AVAILABLE_SABlurImageView
#define COCOAPODS_VERSION_MAJOR_SABlurImageView 1
#define COCOAPODS_VERSION_MINOR_SABlurImageView 1
#define COCOAPODS_VERSION_PATCH_SABlurImageView 1

// Spring
#define COCOAPODS_POD_AVAILABLE_Spring
#define COCOAPODS_VERSION_MAJOR_Spring 1
#define COCOAPODS_VERSION_MINOR_Spring 0
#define COCOAPODS_VERSION_PATCH_Spring 3

// SwiftyJSON
#define COCOAPODS_POD_AVAILABLE_SwiftyJSON
#define COCOAPODS_VERSION_MAJOR_SwiftyJSON 2
#define COCOAPODS_VERSION_MINOR_SwiftyJSON 2
#define COCOAPODS_VERSION_PATCH_SwiftyJSON 1

// Toucan
#define COCOAPODS_POD_AVAILABLE_Toucan
#define COCOAPODS_VERSION_MAJOR_Toucan 0
#define COCOAPODS_VERSION_MINOR_Toucan 3
#define COCOAPODS_VERSION_PATCH_Toucan 1

// UIImage-Resize
#define COCOAPODS_POD_AVAILABLE_UIImage_Resize
#define COCOAPODS_VERSION_MAJOR_UIImage_Resize 1
#define COCOAPODS_VERSION_MINOR_UIImage_Resize 0
#define COCOAPODS_VERSION_PATCH_UIImage_Resize 1

// ZFDragableModalTransition
#define COCOAPODS_POD_AVAILABLE_ZFDragableModalTransition
#define COCOAPODS_VERSION_MAJOR_ZFDragableModalTransition 0
#define COCOAPODS_VERSION_MINOR_ZFDragableModalTransition 5
#define COCOAPODS_VERSION_PATCH_ZFDragableModalTransition 1

