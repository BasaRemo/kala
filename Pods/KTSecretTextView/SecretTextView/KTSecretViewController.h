//
//  KTSecretViewController.h
//
//  Created by Kenny Tang on 4/30/14.
//  Copyright (c) 2014 Corgitoergosum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTSecretTextView.h"
#import "KTSecretColorChooserViewController.h"
#import "KTSecretPhotosEditorViewController.h"
#import "KTSecretTextureChooserViewController.h"

@protocol CloseDoneBtnDelegate <NSObject>

@optional
- (void)closeBtnSelected;
- (void)doneBtnSelected:(NSString *)backgroundColor :(NSString *)textureName;
- (void)doneBtnSelected;
@required


@end

@protocol KTSecretViewControllerDelegate;

/*!
 *  View Controller that hosts the secret view.
 */
@interface KTSecretViewController : UIViewController

@property (nonatomic, weak) id<KTSecretViewControllerDelegate> delegate;
@property id<CloseDoneBtnDelegate> closeDoneDelegate;
// text view
@property (nonatomic, strong) KTSecretTextView *textView;
@property (nonatomic, strong) NSString *currentColor;
@property (nonatomic, strong) NSString *currentTexture;

// background color
@property (nonatomic, strong) KTSecretColorChooserViewController *colorChooserViewController;

// textures
@property (nonatomic, strong) KTSecretTextureChooserViewController *textureChooserViewController;


@end


@protocol KTSecretViewControllerDelegate <NSObject>

- (void)secretViewController:(KTSecretViewController*)vc secretViewSnapshot:(UIView*)snapshotView backgroundImage:(UIImage*)backgroundImage attributedString:(NSAttributedString*)attributedString;

@end

