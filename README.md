Palaver 

Palaver is a IOS app for creative minds. Collaborate to create new stories with friends, contribute to ongoing stories around the world and watch your story take unexpected twist!