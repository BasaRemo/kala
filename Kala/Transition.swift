//
//  Transition.swift
//  Kala
//
//  Created by Professional on 2015-07-25.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import Foundation
import Parse

class Transition : PFObject, PFSubclassing {
    
    @NSManaged var successor: String?
    @NSManaged var content: String?
    
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "Transition"
    }
    
    override init () {
        super.init()
    }
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }
    
}