//
//  Story.swift
//  Kala
//
//  Created by Professional on 2015-07-22.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import Foundation
import Parse

class Story : PFObject, PFSubclassing {
    
    @NSManaged var content: String?
    @NSManaged var user: PFUser?
    @NSManaged var completed: NSNumber?
    
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "Story"
    }
    
    override init () {
        super.init()
    }
    
    func uploadStory() {
        self.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                // The score key has been incremented
                println("Success Creating Fragment")
            } else {
                // There was a problem, check error.description
                println("Error Creating Fragment")
            }
        }
    }
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }
    
}