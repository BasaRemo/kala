//
//  FragmentCell.swift
//  Kala
//
//  Created by Professional on 2015-07-31.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import RQShineLabel
import Bond
import Parse
import DOFavoriteButton

class FragmentCell: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var collectionViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var contentLabel: RQShineLabel!
    @IBOutlet var textureImage: UIImageView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet var contributeButton: UIButton!
    @IBOutlet var likeButton: DOFavoriteButton!
    weak var timeline: FragmentsVC?
    var likeBond: Bond<[PFUser]?>!

    var rowIndex:Int = 0 {
        didSet {
            if let fragment = fragment {
                likeButton.tag = rowIndex
            }
        }
    }
    var fragment:Fragment? {
        didSet {
            if let fragment = fragment {
                // bind the likeBond that we defined earlier, to update like label and button when likes change
                fragment.likes ->> likeBond
                self.contentLabel.text = fragment.content ?? ""
                if fragment.colorName != nil {
                    setCellBackgroundColor(fragment.colorName!)
                }else {
                    setCellBackgroundColor("EmeraldSea")
                }
                
                if fragment.textureName != nil {
                    setCellTextureImage(fragment.textureName!)
                }else {
                    setCellTextureImage("Noise")
                    self.textureImage.image = TEXTURES_DICTIONNARY["Noise"]
                }
                self.contributeButton.titleLabel?.textAlignment = NSTextAlignment.Center
                
                if (fragment.completed == true) {
                    self.contributeButton.titleLabel?.text = "Read"
                }
                
            }
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let fragment  = self.fragment {
            
            if (fragment.completed == true) {
                self.contributeButton.titleLabel?.text = "Read"
            }
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentLabel.fadeoutDuration = 0.0
        self.contentLabel.shineDuration = 1.0
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.likeButton.addTarget(self, action: Selector("tappedButton:"), forControlEvents: UIControlEvents.TouchUpInside)
        self.contentLabel.layer.shadowColor = UIColor.blackColor().CGColor
        self.contentLabel.layer.shadowOffset = CGSizeMake(1, 1)
        self.contentLabel.layer.shadowRadius = 1
        self.contentLabel.layer.shadowOpacity = 1.0
        
        self.contentLabel.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        
        // Initialization code
        //self.contentLabel.numberOfLines = 10
        //self.contentLabel.sizeToFit()
        //self.contentLabel.textColor = UIColor.blackColor()
        //cell.contentLabel.backgroundColor = UIColor.blackColor()
        
    }
    //MARK: Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        likeBond = Bond<[PFUser]?>() { [unowned self] likeList in
            if let likeList = likeList {
                self.likesLabel.text = String(likeList.count)
                self.likeButton.selected = contains(likeList, PFUser.currentUser()!)
                //self.likesIconImageView.hidden = (likeList.count == 0)
            } else {
                // if there is no list of users that like this post, reset everything
                self.likesLabel.text = "0"
                self.likeButton.selected = false
                //self.likesIconImageView.hidden = true
            }
        }
        
        if let fragment  = self.fragment {
            
            if (fragment.completed == true) {
                self.contributeButton.titleLabel?.text = "Read"
            }
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Helper functions
    
    // Generates a comma seperated list of usernames from an array (e.g. "User1, User2")
    func stringFromUserlist(userList: [PFUser]) -> String {
        let usernameList = userList.map { user in user.username! }
        let commaSeperatedUserList = ", ".join(usernameList)
        
        return commaSeperatedUserList
    }
    
    func setCellBackgroundColor(color:String){
        self.contentView.backgroundColor = COLORS_DICTIONNARY[color]
    }
    func setCellTextureImage(imageString:String){
        self.textureImage.image = TEXTURES_DICTIONNARY[imageString]
    }
    
    // MARK: Button Callbacks
    func tappedButton(sender: DOFavoriteButton) {
        
            if sender.selected {
                sender.deselect()
            } else {
                sender.select()
            }
            self.fragment?.toggleLikePost(PFUser.currentUser()!)

    }
    @IBAction func moreButtonTapped(sender: AnyObject) {
        timeline?.showActionSheetForPost(fragment!)
    }
    
    @IBAction func contributeToStory(sender: CustomButton) {
        println("TEXT: \(sender.titleLabel?.text)")
        
        if let fragment  = self.fragment {
            
            if (fragment.completed == true) {
                timeline?.ShowReadStoryView(rowIndex)
                self.contributeButton.titleLabel?.text = "Read"
            }else{
                timeline?.ShowContributeView(rowIndex)
            }
        }

        
    }
}

//Mark - UICollectionViewDataSource
extension FragmentCell:UICollectionViewDataSource{

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("contributorCell", forIndexPath: indexPath) as! UICollectionViewCell
        
        cell.layer.cornerRadius = cell.frame.width/2
        var imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:"pic.png")
        //(cell.contentView.viewWithTag(11) as! UILabel).text = iconsLabels[indexPath.row]
        
        return cell
    }
}
//Mark - UICollectionViewDelegate
extension FragmentCell:UICollectionViewDelegate{
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            println("\(indexPath.row)")
            
            self.collectionView.layoutIfNeeded()
            UIView.animateWithDuration(2.0, delay: 0, options: .CurveEaseInOut, animations: { () -> Void in
                self.collectionViewWidthConstraint.constant = 200
                self.collectionView.layoutIfNeeded()
                }, completion: nil)
//            UIView.animateWithDuration(2.0, animations: { () -> Void in
//                self.collectionViewWidthConstraint.constant = 200
//                collectionView.layoutIfNeeded()
//            })
    
    }
}
