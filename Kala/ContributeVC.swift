//
//  ContributeVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import KTSecretTextView
import Parse

class ContributeVC: KTSecretViewController {

    var content:String?
    var fragmentLength: Int?
    var fragment:Fragment?
    var colorIndex: CGFloat = 0
    var currentOrder : Int = 0
    var isStory:Bool = false
    var maxLength = 140
    var Xpos = 0
    var Ypos = 0
    var label:UILabel?
    var descriptionLabel:UILabel?
    //@IBOutlet weak var textView: PlaceholderTextView!
    var secretVC: KTSecretViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.closeDoneDelegate = self
        self.textView.delegate = self
        
        //Add Char limit label
        label = UILabel(frame: CGRectMake(0, 200, 200, 30))
        if let label = self.label{
            
            label.font = label.font.fontWithSize(13)
            label.textColor = UIColor.whiteColor()
            label.center = self.view.center
            label.textAlignment = NSTextAlignment.Center
            label.text = "0"
            self.view.addSubview(label)
            
            
            label.setTranslatesAutoresizingMaskIntoConstraints(false)
            let labelHorizontalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 135)
            self.view.addConstraint(labelHorizontalConstraint)
            
            let labelVerticalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -260)
            self.view.addConstraint(labelVerticalConstraint)
            
            let labelWidthConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
            self.view.addConstraint(labelWidthConstraint)
            
            let labelHeightConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
            self.view.addConstraint(labelHeightConstraint)
        }
        
        
        self.descriptionLabel = UILabel(frame: CGRectMake(0, 330, 200, 20))
        
        if let descriptionLabel = self.descriptionLabel {
            
            descriptionLabel.font = descriptionLabel.font.fontWithSize(13)
            descriptionLabel.textColor = UIColor.whiteColor()
            descriptionLabel.center = self.view.center
            descriptionLabel.textAlignment = NSTextAlignment.Center
            descriptionLabel.text = "/\(maxLength)"
            self.view.addSubview(descriptionLabel)
            
            descriptionLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            let descriptionHorizontalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 160)
            self.view.addConstraint(descriptionHorizontalConstraint)
            
            let descriptionVerticalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -260)
            self.view.addConstraint(descriptionVerticalConstraint)
            
            let descriptionWidthConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
            self.view.addConstraint(descriptionWidthConstraint)
            
            let descriptionHeightConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
            self.view.addConstraint(descriptionHeightConstraint)
        }
        
        self.textView.becomeFirstResponder()
    
    }
    override func viewWillAppear(animated: Bool)
    {
        self.textView.text = fragment?.content
        self.fragmentLength = fragment?.content?.length
        
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        self.colorChooserViewController.collectionView.layoutIfNeeded()
        var newIndex = NSIndexPath(index: 1)
        var collectionView:UICollectionView = self.colorChooserViewController.collectionView!
        self.colorChooserViewController.collectionView.scrollRectToVisible(CGRectMake(collectionView.frame.size.width*colorIndex,0,collectionView.frame.size.width,collectionView.frame.size.height), animated: false)
        self.colorChooserViewController.collectionView!.scrollEnabled = false
        self.scrollToCursorPosition()
        if (self.fragment?.completed == true) {
            self.textView.editable = false
        }

    }
    
    func createFragment() {
        
        self.currentOrder = self.fragment!.order!.integerValue
        if self.currentOrder < 10 {
            self.getNextTransition()
        }else{
           if self.currentOrder == 10 {
                let fragment = Fragment()
                var newContent = self.textView.text
                fragment.content = newContent
                fragment.user = PFUser.currentUser()
                fragment.order = self.currentOrder
                fragment.textureName = self.fragment?.textureName
                fragment.colorName = self.fragment?.colorName
                fragment.setValue(NSNumber(bool: false), forKey: "hidden")
                fragment.setValue(self.fragment!.shareToWorld, forKey: "shareToWorld")
                
                // Create a pointer to an object of class Story with id of the fragment story
                var storyPointer = PFObject(withoutDataWithClassName:"Story", objectId:self.fragment?.story!.objectId)
                var storyContent = newContent
                storyPointer.setValue(storyContent, forKey: "content")
                
                storyPointer.setValue(NSNumber(bool: true), forKey: "completed")
                fragment.setValue(NSNumber(bool: true), forKey: "completed")
                newContent = self.textView.text
                
                storyPointer.save()
                
                //Assign the pointer to the fragment story and upload the fragment
                fragment["story"] = storyPointer
                fragment.uploadFragment()
                
                //hide previous fragment
                var previousFragment = PFObject(withoutDataWithClassName:"Fragment", objectId:self.fragment?.objectId)
                previousFragment.setValue(NSNumber(bool: true), forKey: "hidden")
                previousFragment.save()
            }
            
//            // Congrats Alert View
//            let alertView = UIAlertController(title: "Congrats!", message: "You finished your story. You can now find it in the stories tab.", preferredStyle: .Alert)
//            alertView.addAction(UIAlertAction(title: "Dude!", style: .Default, handler: nil))
//            presentViewController(alertView, animated: true, completion: nil)
        }
        
        
    }
    
    private func scrollToCursorPosition() {
        let caret = self.textView.caretRectForPosition(textView.selectedTextRange!.start)
        textView.scrollRectToVisible(caret, animated: true)
    }
    
    //Query Transition that can begin a story
    func getNextTransition(){
        
        let transitionQuery = Transition.query()
        transitionQuery!.whereKey("order", equalTo: NSNumber(integer:currentOrder++))
        transitionQuery!.findObjectsInBackgroundWithBlock {(result: [AnyObject]?, error: NSError?) -> Void in
            
            var transitions = result as? [Transition] ?? []
            //We take the 1st transition of a particular order for now
            
            if let nextTransition = transitions[0].content {
                
                let fragment = Fragment()
                var newContent = self.textView.text /*+ "\n \(nextTransition)"*/
                fragment.content = newContent
                fragment.user = PFUser.currentUser()
                fragment.order = self.currentOrder
                fragment.textureName = self.fragment?.textureName
                fragment.colorName = self.fragment?.colorName
                fragment.setValue(NSNumber(bool: false), forKey: "hidden")
                fragment.setValue(self.fragment!.shareToWorld, forKey: "shareToWorld")
                
                // Create a pointer to an object of class Story with id of the fragment story
                var storyPointer = PFObject(withoutDataWithClassName:"Story", objectId:self.fragment?.story!.objectId)
                var storyContent = newContent
                storyPointer.setValue(storyContent, forKey: "content")
                storyPointer.save()
                
                //Assign the pointer to the fragment story and upload the fragment
                fragment["story"] = storyPointer
                fragment.uploadFragment()
                
                //hide previous fragment
                var fragmentPointer = PFObject(withoutDataWithClassName:"Fragment", objectId:self.fragment?.objectId)
                fragmentPointer.setValue(NSNumber(bool: true), forKey: "hidden")
                fragmentPointer.save()
            }
            
        }
    }
    
    @IBAction func updateFragment(sender: CustomButton) {
        
        getNextTransition()
    }

}
extension ContributeVC: UITextViewDelegate {
    
    func changeInputTextColor(){
        
        let range = NSMakeRange(self.fragmentLength!, count(self.textView.text) - self.fragmentLength! )
        let string = NSMutableAttributedString(attributedString: self.textView.attributedText)
        var font = UIFont(name: "AvenirNextCondensed-Medium", size: 24.0)!
        let attributes = [NSForegroundColorAttributeName: UIColor.redColor(), NSFontAttributeName: font]
        string.addAttributes(attributes, range: range)
        //string.addAttribute(NSFontAttributeName, value: UIFont(name: "AvenirNextCondensed-Medium", size: 24.0)!, range: range)
        textView.attributedText = string
        
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        var contentSize = self.fragment?.content!.length ?? 0
        
        let newLength = count(textView.text.utf16) - contentSize + count(text.utf16) - range.length
        //change the value of the label
        
        if let label = self.label {
            
            label.text?.removeAll(keepCapacity: false)
            label.text? =  String(newLength)
        }

        
        if range.location < contentSize{
            return false
        }
        if textView.text.length + ((text.length - contentSize)  - range.length) >= maxLength {
            return false
        }

        
        changeInputTextColor()
        
        return true
    }
}
extension ContributeVC: CloseDoneBtnDelegate {
    
    func doneBtnSelected(backgroundColor: String!, _ textureName: String!) {
        println("Done Update Selected")
        createFragment()
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}