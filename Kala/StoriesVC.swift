//
//  StoriesVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class StoriesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var stories:[Story]?
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(animated: Bool)
    {
        // Do any additional setup after loading the view.
        let storyQuery = Story.query()
        storyQuery!.whereKey("completed", equalTo: NSNumber(bool: true))
        storyQuery!.orderByDescending("createdAt")
        storyQuery!.findObjectsInBackgroundWithBlock {(result: [AnyObject]?, error: NSError?) -> Void in
            
            self.stories = result as? [Story] ?? []
            self.tableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StoriesVC: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("StoryCell", forIndexPath: indexPath) as! StoryCell
        if (stories?.count > 0){
            
            if (stories?[indexPath.row].content != nil) {
                cell.contentLabel?.text = stories?[indexPath.row].content
            }
            
        }
        
    return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return stories?.count ?? 0
    }

}

extension StoriesVC: UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }

    // 3
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
    }

    // 4
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell:StoryCell = (cell as? StoryCell)!
        cell.contentLabel.shine()
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell:StoryCell = (cell as? StoryCell)!
    }

}