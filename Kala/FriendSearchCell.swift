//
//  FriendSearchTableViewCell.swift
//  Makestagram
//
//  Created by Benjamin Encz on 6/2/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import Parse

protocol FriendSearchCellDelegate: class {
  func cell(cell: FriendSearchCell, didSelectFollowUser user: PFUser)
  func cell(cell: FriendSearchCell, didSelectUnfollowUser user: PFUser)
}

class FriendSearchCell: UITableViewCell {

  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var followButton: UIButton!
  weak var delegate: FriendSearchCellDelegate?
  
  var user: PFUser? {
    didSet {
      usernameLabel.text = user?.username
    }
  }
  
  var canFollow: Bool? = true {
    didSet {
      /* 
        Change the state of the follow button based on whether or not 
        it is possible to follow a user.
      */
      if let canFollow = canFollow {
        followButton.selected = !canFollow
      }
    }
  }
  
  @IBAction func followButtonTapped(sender: AnyObject) {
    if let canFollow = canFollow where canFollow == true {
      delegate?.cell(self, didSelectFollowUser: user!)
      self.canFollow = false
    } else {
      delegate?.cell(self, didSelectUnfollowUser: user!)
      self.canFollow = true
    }
  }
}