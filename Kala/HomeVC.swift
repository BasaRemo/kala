//
//  HomeVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import LGPlusButtonsView
import CoreImage
import Toucan

class HomeVC: UIViewController {
    
    var plusButtonView: LGPlusButtonsView!
    @IBOutlet var segmentControl : ADVSegmentedControl!
    @IBOutlet var fragmentContainer: UIView!
    @IBOutlet var storyContainer: UIView!
    var imageView:UIImageView?
    @IBOutlet var menuViewContainer: UIView!
    
    var storiesVC:StoriesVC?
    var fragmentsVC:FragmentsVC?
    var screenShot:UIImage?
    //@IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fragmentContainer.hidden = false
        self.storyContainer.hidden = true
        
        self.fragmentContainer.backgroundColor = defaultColor
        self.storyContainer.backgroundColor = defaultColor
        
        self.initPlusButtonView()
        self.setupPlusButtons()
        setSegmentControl()

    }
    
    func setSegmentControl(){
        
        segmentControl.items = ["World", "Friends"]
        segmentControl.font = UIFont(name: "Avenir-Black", size: 12)
        segmentControl.borderColor = UIColor(white: 1.0, alpha: 0.3)
        segmentControl.selectedIndex = 0
        segmentControl.addTarget(self, action: "segmentValueChanged:", forControlEvents: .ValueChanged)
    }
    func takeScreenShot() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.fragmentContainer.frame.size, false, 0)
        var image:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        self.view?.drawViewHierarchyInRect(self.fragmentContainer.frame, afterScreenUpdates: true)
        var screenShot  = UIGraphicsGetImageFromCurrentImageContext()
        return screenShot
    }
    
    func segmentValueChanged(sender: AnyObject?){
        
//        self.screenShot = takeScreenShot()
//        self.fragmentContainer.backgroundColor = UIColor(patternImage: screenShot!)
//        self.storyContainer.backgroundColor = UIColor(patternImage: screenShot!)
        
        //ANIMATION TRANSITION
        let transition = CircularRevealTransition(layer: self.fragmentsVC!.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
        transition.start()

        if segmentControl.selectedIndex == 0 {
            
            self.fragmentsVC!.filter = "Global"
            self.fragmentsVC!.timelineComponent.removeAllContent()
            self.fragmentsVC!.timelineComponent.loadInitialIfRequired()
//            let transition = CircularRevealTransition(layer: self.fragmentsVC!.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
//            transition.start()
//            self.fragmentContainer.hidden = !self.fragmentContainer.hidden
//            self.storyContainer.hidden = !self.storyContainer.hidden

        }else if segmentControl.selectedIndex == 1{
            
            self.fragmentsVC!.filter = "Friends"
            self.fragmentsVC!.timelineComponent.removeAllContent()
            self.fragmentsVC!.timelineComponent.loadInitialIfRequired()
//            let transition = CircularRevealTransition(layer: self.storiesVC!.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
//            transition.start()
//            self.fragmentContainer.hidden = !self.fragmentContainer.hidden
//            self.storyContainer.hidden = !self.storyContainer.hidden
        }
    }
    @IBAction func selectedMenuBtn(sender: UIButton) {
        
        self.plusButtonsViewPlusButtonPressed(plusButtonView)
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.tabBarController?.tabBar.hidden = false
        //UIApplication.sharedApplication().statusBarHidden=true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func unwindToMainMenu(segue: UIStoryboardSegue) {
        
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FragmentSegue" {
            let viewController = segue.destinationViewController as? FragmentsVC
            self.fragmentsVC = viewController
            
        }
        else if segue.identifier == "StorySegue"{
            
            let viewController = segue.destinationViewController as? StoriesVC
            self.storiesVC = viewController
        }
    }

}

// MARK: LGPlusButtonSetup
extension HomeVC: LGPlusButtonsViewDelegate
{
    func plusButtonsViewPlusButtonPressed(plusButtonsView: LGPlusButtonsView!) {
        showOrHideCategories()
    }
    
    func plusButtonsView(plusButtonsView: LGPlusButtonsView!, buttonPressedWithTitle title: String!, description: String!, index: UInt)
    {
        switch index
        {
        case 0:
            println("button 0 ")
            showOrHideCategories()
        case 1:
            println("button 1")
            showOrHideCategories()
        case 2:
            println("button 2")
            showOrHideCategories()
            
        default: println("whoops")
        }
        
//        self.screenShot = takeScreenShot()
//        self.fragmentContainer.backgroundColor = UIColor(patternImage: self.screenShot!)
//        self.storyContainer.backgroundColor = UIColor(patternImage: self.screenShot!)
        
        if !self.fragmentContainer.hidden {
            let transition = CircularRevealTransition(layer: self.fragmentsVC!.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
            transition.start()

        }else{
            let transition2 = CircularRevealTransition(layer: self.storiesVC!.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
            transition2.start()
        }
    }
    
    func setupPlusButtons()
    {
        if plusButtonView.buttons == nil { return }
        
        plusButtonView.plusButton = plusButtonView.buttons.firstObject as! LGPlusButton
        
        let isPortrait = UIInterfaceOrientationIsPortrait(UIApplication.sharedApplication().statusBarOrientation)
        
        let buttonSide:CGFloat = (isPortrait ? 44.0 : 36.0)
        let inset:CGFloat = (isPortrait ? 3.0 : 2.0)
        let buttonsFontSize:CGFloat = (isPortrait ? 15.0 : 10.0)
        let plusButtonFontSize:CGFloat = buttonsFontSize*1.5 // TODO figure out why the font on first button is different
        
        plusButtonView.buttonInset = UIEdgeInsetsMake(inset, inset, inset, inset)
        plusButtonView.contentInset = UIEdgeInsetsMake(inset, inset, inset, inset)
        plusButtonView.offset = CGPointMake(-10.7, 85.0)
        
        plusButtonView.setButtonsTitleFont(UIFont.boldSystemFontOfSize(buttonsFontSize))
        
        plusButtonView.plusButton.titleLabel?.font = UIFont.systemFontOfSize(plusButtonFontSize)
        plusButtonView.plusButton.titleOffset = CGPointMake(0.0, -plusButtonFontSize*0.1);
        
        plusButtonView.buttonsSize = CGSizeMake(buttonSide, buttonSide);
        plusButtonView.setButtonsLayerCornerRadius(buttonSide/2)
        plusButtonView.setButtonsLayerBorderColor(defaultColor, borderWidth: 0.0)
        //        plusButtonView.backgroundColor = UIColor.whiteColor()
        plusButtonView.setButtonsLayerMasksToBounds(true)
        
        var whiteImage = getImageWithColor(UIColor.blackColor(), size: CGSize(width: 44.0,height: 44.0))
        
        for (var i=0; i<plusButtonView.buttons.count; i++)
        {
            switch i {
            case 0:
                plusButtonView.buttons[i].setImage(UIImage(named: "marker"), forState: UIControlState.Normal)
                plusButtonView.buttons[i].setBackgroundImage(whiteImage, forState: UIControlState.Normal)
            case 1:
                plusButtonView.buttons[i].setImage(UIImage(named: "world"),          forState: UIControlState.Normal)
                plusButtonView.buttons[i].setBackgroundImage(whiteImage, forState: UIControlState.Normal)
            case 2:
                plusButtonView.buttons[i].setImage(UIImage(named: "group"),         forState: UIControlState.Normal)
                plusButtonView.buttons[i].setBackgroundImage(whiteImage, forState: UIControlState.Normal)
                //                plusButtonView.buttons[i].cornerRadius = 22.0
            default: println("something went wrong along the way");
            }
        }
    }
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func initPlusButtonView()
    {
        plusButtonView = LGPlusButtonsView(view: self.view, numberOfButtons: 3,showsPlusButton: false, actionHandler:
            { (view:LGPlusButtonsView!, title:String!, description:String!, index: UInt) -> Void in
                println("\(title), \(description), \(index)")
            }, plusButtonActionHandler: nil)
        
        plusButtonView.delegate = self
        
        plusButtonView.setDescriptionsTexts(["Current Location", "Global", "Friends"])
        plusButtonView.position = LGPlusButtonsViewPositionTopRight
        plusButtonView.appearingAnimationType = LGPlusButtonsAppearingAnimationTypeCrossDissolveAndPop
        plusButtonView.setButtonsTitleColor(UIColor.whiteColor(), forState:UIControlState.Normal)
        plusButtonView.setButtonsAdjustsImageWhenHighlighted(false)
        
    }
    
    func showOrHideCategories() // efficient function to show or hide the plusbuttonview
    {
        if (plusButtonView.showing)
        {
            plusButtonView.hideAnimated(true, completionHandler:nil);
        }
        else
        {
            plusButtonView.showAnimated(true, completionHandler:nil);
        }
    }
}

