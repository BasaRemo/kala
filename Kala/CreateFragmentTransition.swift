//
//  CreateFragmentTransition.swift
//  Kala
//
//  Created by Professional on 2015-07-22.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class CreateFragmentTransition: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        toViewController.view.alpha = 0.5
        containerView.addSubview(toViewController.view)
        containerView.sendSubviewToBack(toViewController.view)
        
        let snapshotView = fromViewController.view.snapshotViewAfterScreenUpdates(false)
        snapshotView.frame = fromViewController.view.frame
        containerView.addSubview(snapshotView)
        fromViewController.view.removeFromSuperview()
        
        UIView.animateWithDuration(0.2, animations: {
            
            snapshotView.frame = CGRectMake(0 , 100, fromViewController.view.frame.width, 321)
            toViewController.view.alpha = 1.0
            
            }, completion: {
                (value: Bool) in
                snapshotView.removeFromSuperview()
                transitionContext.completeTransition(true)
        })
        
    }
    
}