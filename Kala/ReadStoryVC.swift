//
//  ReadStoryVC.swift
//  Kala
//
//  Created by Professional on 2015-08-13.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class ReadStoryVC: UIViewController {

    var colorIndex: CGFloat = 0
    var fragment:Fragment?
    @IBOutlet var textView: UITextView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.containerView.backgroundColor = COLORS_DICTIONNARY[fragment!.colorName!]
        self.textView.text = fragment?.content
    }

    @IBAction func closeView(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
