//
//  StoryCell.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import RQShineLabel

class StoryCell: UITableViewCell {

    @IBOutlet var contentLabel: RQShineLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentLabel.fadeoutDuration = 0.0
        self.contentLabel.shineDuration = 0.5
        self.contentLabel.shine()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
