//
//  Fragment.swift
//  Kala
//
//  Created by Professional on 2015-07-22.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import Foundation
import Parse
import Bond

class Fragment : PFObject, PFSubclassing {

     @NSManaged var content: String?
     @NSManaged var order: NSNumber?
     @NSManaged var user: PFUser?
     @NSManaged var story: Story?
     @NSManaged var hidden: NSNumber?
    @NSManaged var completed: NSNumber?
    @NSManaged var shareToWorld: NSNumber?
     @NSManaged var colorName: String?
     @NSManaged var textureName: String?
     var likes =  Dynamic<[PFUser]?>(nil)
    
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "Fragment"
    }
    
    override init () {
        super.init()
    }
    
    func uploadFragment() {
        self.user = PFUser.currentUser()
        self.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                // The score key has been incremented
                println("Success Creating Fragment")
            } else {
                // There was a problem, check error.description
                println("Error Creating Fragment")
            }
        }
    }
    
    func updateFragment() {
        var query = Fragment.query()
        query!.getObjectInBackgroundWithId(self.objectId!) {
            (transition: PFObject?, error: NSError?) -> Void in
            if error != nil {
                println(error)
            } else if let transition = transition {
                
                //transition["content"] = self.content
                //transition["order"] = self.order!.integerValue + 1
                
                self.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError?) -> Void in
                    if (success) {
                        // The score key has been incremented
                        println("Success Update")
                    } else {
                        // There was a problem, check error.description
                         println("Error Updating Fragment")
                    }
                }
            }
        }
        
    }
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }
    
    func fetchLikes() {
        if (likes.value != nil) {
            return
        }
        
        ParseHelper.likesForPost(self, completionBlock: { (var likes: [AnyObject]?, error: NSError?) -> Void in
            if let error = error {
                ErrorHandling.defaultErrorHandler(error)
            }
            // filter likes that are from users that no longer exist
            likes = likes?.filter { like in like[ParseHelper.ParseLikeFromUser] != nil }
            
            self.likes.value = likes?.map { like in
                let like = like as! PFObject
                let fromUser = like[ParseHelper.ParseLikeFromUser] as! PFUser
                
                return fromUser
            }
        })
    }
    
    //MARK: Likes
    
    func doesUserLikePost(user: PFUser) -> Bool {
        if let likes = likes.value {
            return contains(likes, user)
        } else {
            return false
        }
    }
    
    func toggleLikePost(user: PFUser) {
        if (doesUserLikePost(user)) {
            // if image is liked, unlike it now
            likes.value = likes.value?.filter { $0 != user }
            ParseHelper.unlikePost(user, post: self)
        } else {
            // if this image is not liked yet, like it now
            likes.value?.append(user)
            ParseHelper.likePost(user, post: self)
        }
    }
    
    //MARK: Flagging
    
    func flagPost(user: PFUser) {
        ParseHelper.flagPost(user, post: self)
    }

}