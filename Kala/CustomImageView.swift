//
//  CustomImageView.swift
//  Kala
//
//  Created by Professional on 2015-08-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
@IBDesignable

class CustomImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // Setup the view appearance
    private func setupView(){
        
        //// Bezier 2 Drawing
        var bezier2Path = UIBezierPath()
        bezier2Path.moveToPoint(CGPointMake(415, 0.01))
        bezier2Path.addCurveToPoint(CGPointMake(415, 265), controlPoint1: CGPointMake(415, 0), controlPoint2: CGPointMake(415, 265))
        bezier2Path.addCurveToPoint(CGPointMake(0, 265), controlPoint1: CGPointMake(415, 265), controlPoint2: CGPointMake(212.5, 181.8))
        bezier2Path.addLineToPoint(CGPointMake(0, 0))
        bezier2Path.addLineToPoint(CGPointMake(415, 0))
        bezier2Path.addLineToPoint(CGPointMake(415, 0.01))
        bezier2Path.closePath()

        var shape:CAShapeLayer = CAShapeLayer()
        shape.path = bezier2Path.CGPath;
        self.layer.mask = shape;
        self.layer.masksToBounds = true
        
//        self.layer.cornerRadius = roundness
//        self.layer.borderWidth = customBorderWidth
//        self.layer.borderColor = customBorderWidthColor.CGColor
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
}
