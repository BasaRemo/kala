//
//  FragmentsVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import BubbleTransition
import PullToMakeFlight
import Parse
import ZFDragableModalTransition

let presentPopUpTransition = PresentPopUpTransition()
let dismissPopUpTransition = DismissPopUpTransition()

class FragmentsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var fragments:[Fragment]?
    var stories:[Story]?
    var data = []
    let transition = BubbleTransition()
    let refresher = PullToMakeFlight()
    
    // Timeline Component Protocol
    let defaultRange = 0...20
    let additionalRangeSize = 20
    var timelineComponent: TimelineComponent<Fragment, FragmentsVC>!
    var filter:String = "Global"
    var animator:ZFModalTransitionAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.timelineComponent = TimelineComponent(target: self)

    }

    override func viewWillAppear(animated: Bool)
    {
        // Do any additional setup after loading the view.
        timelineComponent.loadInitialIfRequired()
        
    }

    //Mark - Show Contribute View
    func ShowContributeView(rowIndex:Int){
        
        let viewController = ContributeVC()
        var fragment = self.timelineComponent.content[rowIndex]
        viewController.fragment = fragment
        if fragment.colorName != nil {
            viewController.colorIndex = CGFloat(COLORS_INDEX[fragment.colorName!]!)
        }
        
        presentViewController(viewController, animated: true, completion: nil)
        
    }

    //Mark - Show Contribute View
    func ShowReadStoryView(rowIndex:Int){
        
        if let viewController = storyboard!.instantiateViewControllerWithIdentifier("ReadStoryVC") as? ReadStoryVC {
            var fragment = self.timelineComponent.content[rowIndex]
            viewController.fragment = fragment
            self.animator = ZFModalTransitionAnimator(modalViewController: viewController)
            self.animator.dragable = true
            self.animator.bounces = false
            self.animator.behindViewAlpha = 0.5
            self.animator.behindViewScale = 1.0
            self.animator.transitionDuration = 0.7
            self.animator.direction = ZFModalTransitonDirection.Bottom
            self.animator.setContentScrollView(viewController.textView)
            
            viewController.transitioningDelegate = self.animator
            viewController.modalPresentationStyle = UIModalPresentationStyle.Custom
            
            if fragment.colorName != nil {
                viewController.colorIndex = CGFloat(COLORS_INDEX[fragment.colorName!]!)
            }
            presentViewController(viewController, animated: true, completion: nil)
        }
        
    }

    // MARK: UIActionSheets
    
    func showActionSheetForPost(post: Fragment) {
        if (post.user == PFUser.currentUser()) {
            showDeleteActionSheetForPost(post)
        } else {
            showFlagActionSheetForPost(post)
        }
    }
    
    func showDeleteActionSheetForPost(post: Fragment) {
        let alertController = UIAlertController(title: nil, message: "Do you want to delete this post?", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Delete", style: .Destructive) { (action) in
            post.deleteInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                if (success) {
                    self.timelineComponent.removeObject(post)
                } else {
                    // restore old state
                    self.timelineComponent.refresh(self)
                }
            })
        }
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showFlagActionSheetForPost(post: Fragment) {
        let alertController = UIAlertController(title: nil, message: "Do you want to flag this post?", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Flag", style: .Destructive) { (action) in
            post.flagPost(PFUser.currentUser()!)
        }
        
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}

// MARK: TimelineComponentTarget implementation
extension FragmentsVC: TimelineComponentTarget{

    func loadInRange(range: Range<Int>, completionBlock: ([Fragment]?) -> Void) {
        ParseHelper.timelineRequestforCurrentUser(range,filter:filter) {
            (result: [AnyObject]?, error: NSError?) -> Void in
            if let error = error {
                ErrorHandling.defaultErrorHandler(error)
            }
            
            let posts = result as? [Fragment] ?? []
            completionBlock(posts)
        }
    }
    
    func reload(){
        self.tableView.reloadData()
//        let transition = CircularRevealTransition(layer: self.view.layer, center: CGPointMake(self.view.frame.size.width/2, 40))
//        transition.start()
    }
}

extension FragmentsVC: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FragmentCell = tableView.dequeueReusableCellWithIdentifier("FragmentCell", forIndexPath: indexPath) as! FragmentCell
        
        let fragment = timelineComponent.content[indexPath.row]
        fragment.fetchLikes()
        cell.fragment = fragment
        cell.timeline = self
        cell.rowIndex = indexPath.row
        
    return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.timelineComponent.content.count ?? 0
    }

}

extension FragmentsVC: UITableViewDelegate {


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }

    // 3
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
    }

    // 4
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        timelineComponent.targetWillDisplayEntry(indexPath.row)
        let cell:FragmentCell = (cell as? FragmentCell)!
        cell.contentLabel.shine()
        
        if let fragment  = cell.fragment {
            
            if (fragment.completed == true) {
                cell.contributeButton.titleLabel?.text = "Read"
            }
        }

    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell:FragmentCell = (cell as? FragmentCell)!
        if let fragment  = cell.fragment {
            
            if (fragment.completed == true) {
                cell.contributeButton.titleLabel?.text = "Read"
            }
        }

    }
}
extension FragmentsVC:UIViewControllerTransitioningDelegate{
    // MARK: UIViewControllerTransitioningDelegate
    
//    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .Present
//        transition.startingPoint = CGPointMake(self.view.center.x, 40)
//        transition.bubbleColor = defaultColor
//        return transition
//    }
//    
//    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .Dismiss
//        transition.startingPoint = CGPointMake(self.view.center.x, 40)
//        transition.bubbleColor = defaultColor
//        return transition
//    }

    
//    //Mark - Custom modal Transitions animation
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presentPopUpTransition
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissPopUpTransition
    }
}