//
//  PresentPopUpTransition.swift
//  Glance
//
//  Created by Professional on 2015-06-18.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class PresentPopUpTransition: NSObject,UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 0.6
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        /*let snapshotView = UIView()
        //fromViewController.view.snapshotViewAfterScreenUpdates(false)
        snapshotView.backgroundColor = UIColor(patternImage: UIImage(named: "sign_bkg@2x.png")!)
        snapshotView.frame = CGRectMake(0 , 100, fromViewController.view.frame.width, 321)
        containerView.addSubview(snapshotView)*/
        UIGraphicsBeginImageContextWithOptions(fromViewController.view.frame.size, false, 0)
        var image:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        fromViewController.view.drawViewHierarchyInRect(fromViewController.view.frame, afterScreenUpdates: true)
        var screenShot  = UIGraphicsGetImageFromCurrentImageContext()
        
        toViewController.view.frame = CGRectMake(fromViewController.view.frame.origin.x, -fromViewController.view.frame.height, fromViewController.view.frame.width, fromViewController.view.frame.height)
        toViewController.view.backgroundColor = UIColor(patternImage: screenShot)
        containerView.addSubview(toViewController.view)
        //toViewController.view.alpha = 0.0
        
        
//        var screenShotImage:UIImage = fromViewController.view.takeScreenShot()
//        toViewController.view.backgroundColor = UIColor(patternImage: screenShotImage)
//        
        
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            toViewController.view.frame = finalFrameForVC
            //toViewController.view.alpha = 1;
            //fromViewController.view.alpha = 0;
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
   
}
