//
//  TabbarDismissTransition.swift
//  Kala
//
//  Created by Professional on 2015-08-03.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit

class TabbarDismissTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        // Get the "from" and "to" views
        let fromView : UIView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView : UIView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        
        transitionContext.containerView().addSubview(fromView)
        transitionContext.containerView().addSubview(toView)
        
        //The "to" view with start "off screen" and slide left pushing the "from" view "off screen"
        //fromView.frame = CGRectMake(0, 0, 320, 560)
        let fromNewFrame = CGRectMake(-1 * fromView.frame.width, 0, fromView.frame.width, fromView.frame.height)
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
            //fromView.frame = CGRectMake(0, toView.frame.height, toView.frame.width, toView.frame.height)
            fromView.frame = fromNewFrame
            }) { (Bool) -> Void in
                // update internal view - must always be called
                transitionContext.completeTransition(true)
        }
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 2.35
    }
}