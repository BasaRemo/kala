//
//  CustomTabBarController.swift
//  Kala
//
//  Created by Professional on 2015-07-22.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import BubbleTransition

class CustomTabBarController: UITabBarController {

    var selectedTabIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor =  defaultColor
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewControllerForUnwindSegueAction(action: Selector, fromViewController: UIViewController, withSender sender: AnyObject?) -> UIViewController? {
        var resultVC = self.selectedViewController?.viewControllerForUnwindSegueAction(action, fromViewController: fromViewController, withSender: sender)
        return resultVC
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CustomTabBarController : UITabBarDelegate {
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem!) {
        //println("did select item with tag \(item.tag)")
        self.selectedTabIndex = item.tag

    }
}
extension CustomTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(tabBarController: UITabBarController, animationControllerForTransitionFromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        // just return the custom TransitioningObject object that conforms to UIViewControllerAnimatedTransitioning protocol
        //tabBarController.tabBar.selectedItem
        //println("Current tab \(self.selectedIndex)")
        if  self.selectedTabIndex == 1 {
            self.selectedTabIndex = 0
            var animatedTransitioningObject = TabbarTransition()
            return animatedTransitioningObject
            
        }else if self.selectedIndex == 1 {
            var animatedTransitioningObject = TabbarDismissTransition()
            return animatedTransitioningObject
        }
        var animatedTransitioningObject = DefaultTabbarTransition()
        return animatedTransitioningObject

    }
    
}
