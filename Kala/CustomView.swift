//
//  CustomView.swift
//  Kala
//
//  Created by Professional on 2015-08-13.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
@IBDesignable
class CustomView: UIView {
    
    @IBInspectable var roundness: CGFloat = 0.0 {
        didSet{
            setupView()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        //setupView()
        self.layer.masksToBounds = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //setupView()
    }
    
    // Setup the view appearance
    private func setupView(){
        self.layer.masksToBounds = true
        self.layer.cornerRadius = roundness
//        self.layer.borderWidth = customBorderWidth
//        self.layer.borderColor = customBorderWidthColor.CGColor
    }

}
