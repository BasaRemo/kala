//
//  AppDelegate.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import ParseUI

enum COLOR_NAMES {
    
    case KTSecretDefaultColorEmeraldSea
    case KTSecretDefaultColorHopscotch
    case KTSecretDefaultColorLavender
    case KTSecretDefaultColorBurst
    case KTSecretDefaultColorCupid
    case KTSecretDefaultColorPeony
    case KTSecretDefaultColorMidnight
    case KTSecretDefaultColorWhite
}

let COLORS_DICTIONNARY = [
    
    "EmeraldSea":UIColor(red: 18.0/255.0, green: 133.0/255.0, blue: 116.0/255.0, alpha: 1),
    "Hopscotch":UIColor(red: 157.0/255.0, green: 105.0/255.0, blue: 183.0/255.0, alpha: 1),
    "Lavender":UIColor(red: 100.0/255.0, green: 57.0/255.0, blue: 144.0/255.0, alpha: 1),
    "Burst":UIColor(red: 213.0/255.0, green: 100.0/255.0, blue: 12.0/255.0, alpha: 1),
    "Cupid":UIColor(red: 157.0/255.0, green: 30.0/255.0, blue: 38.0/255.0, alpha: 1),
    "Peony":UIColor(red: 220.0/255.0, green: 72.0/255.0, blue: 99.0/255.0, alpha: 1),
    "Midnight":UIColor(red: 39.0/255.0, green: 36.0/255.0, blue: 32.0/255.0, alpha: 1),
    "Default":UIColor.whiteColor()
]
let COLORS_INDEX = [
    
    "EmeraldSea":0,
    "Hopscotch":1,
    "Lavender":2,
    "Burst":3,
    "Cupid":4,
    "Peony":5,
    "Midnight":6,
    "Default":7
]

let TEXTURES_DICTIONNARY = [
    
    "Glow":UIImage(named: "default_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile),
    "Linen":UIImage(named: "linen_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile),
    "Lines":UIImage(named: "lines_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile),
    "Noise":UIImage(named: "noise_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile),
    "Squares":UIImage(named: "squared_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile),
    "Squares 2":UIImage(named: "squares2_texture")!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Tile)
    
]

let defaultColor = UIColor(hex: "178780")
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var parseLoginHelper: ParseLoginHelper!
    
    override init() {
        super.init()
        
        parseLoginHelper = ParseLoginHelper {[unowned self] user, error in
            // Initialize the ParseLoginHelper with a callback
            if let error = error {
                ErrorHandling.defaultErrorHandler(error)
            } else  if let user = user {
                // if login was successful, display the TabBarController
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tabBarController = storyboard.instantiateViewControllerWithIdentifier("CustomTabBarController") as! CustomTabBarController
                
                self.window?.rootViewController!.presentViewController(tabBarController, animated:true, completion:nil)
            }
        }
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        //application.statusBarHidden = true
        // [Optional] Power your app with Local Datastore. For more info, go to
        // https://parse.com/docs/ios_guide#localdatastore/iOS
        //Parse.enableLocalDatastore()
        
        // Initialize Parse.
        Parse.setApplicationId("zDz53ZlTX23x5FyJEL1TgfObRIxmQCpQVzHHivts",
            clientKey: "2jeFWsikHYter8Hl1anmsSUZ0biq9cKOatHwzdqq")
        
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
        
//        // check if we have logged in user
        let user = PFUser.currentUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let startViewController: UIViewController;
        
        if (user != nil) {
            // if we have a user, set the TabBarController to be the initial View Controller
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            startViewController = storyboard.instantiateViewControllerWithIdentifier("CustomTabBarController") as! CustomTabBarController
        } else {
            // Otherwise set the LoginViewController to be the first
//            let loginViewController = PFLogInViewController()
                let loginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as? LoginVC
                loginViewController!.fields = .Facebook
                loginViewController!.delegate = parseLoginHelper
                loginViewController!.signUpController?.delegate = parseLoginHelper
                startViewController = loginViewController!
        }
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = startViewController;
        self.window?.makeKeyAndVisible()
        
        
//        let user = PFUser.currentUser()
//        let startViewController: UIViewController;
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let FacebookUser = FBSDKAccessToken.currentAccessToken()
//        
//        if (user != nil  /*|| FacebookUser != nil*/) {
//
//            // if we have a user, set the CustomTabBarController to be the initial View Controller
//            startViewController = storyboard.instantiateViewControllerWithIdentifier("CustomTabBarController") as! CustomTabBarController
//        } else {
//
//            // Otherwise set the LoginViewController to be the first
//            let loginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as? LoginVC
//                startViewController = loginViewController!
//            loginViewController
//
//        }
//        
//        // 5
//        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
//        self.window?.rootViewController = startViewController
//        self.window?.makeKeyAndVisible()
        
        let acl = PFACL()
        acl.setPublicReadAccess(true)
        PFACL.setDefaultACL(acl, withAccessForCurrentUser: true)
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool { return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

