//
//  ProfileVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Haneke
import Parse
import Alamofire
import SwiftyJSON
import ZFDragableModalTransition

class ProfileVC: UIViewController {

    var fragmentsVC:FragmentsVC?
    @IBOutlet var searchButton: UIButton!{
        didSet {
            if let searchButton = searchButton {
                if  !isUserCurrentProfile {
                    searchButton.hidden = true
                }
            }
        }
    }
    @IBOutlet var backButton:UIButton!{
        didSet {
            if let backButton = backButton {
                if  isUserCurrentProfile {
                    backButton.hidden = true
                }
            }
        }
    }
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var segmentControl : ADVSegmentedControl!
    var stories:[Story]?
    @IBOutlet var backgroundImageView: CustomImageView!
   
    @IBOutlet var userCity: UILabel!
    @IBOutlet var userName: UILabel!
    var isUserCurrentProfile:Bool = true
    var animator:ZFModalTransitionAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        profileImageView.layer.borderWidth = 2.0;
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
        profileImageView.layer.masksToBounds = true;
        
        setSegmentControl()
        loadUserProfile()
        
    }
    override func viewDidAppear(animated: Bool) {
        // Do any additional setup after loading the view.
//        let storyQuery = Story.query()
//        storyQuery!.whereKey("completed", equalTo: NSNumber(bool: true))
//        storyQuery!.orderByDescending("createdAt")
//        storyQuery!.findObjectsInBackgroundWithBlock {(result: [AnyObject]?, error: NSError?) -> Void in
//            
//            self.stories = result as? [Story] ?? []
//            self.tableView.reloadData()
//        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.fragmentsVC!.timelineComponent.removeAllContent()
        self.fragmentsVC!.timelineComponent.loadInitialIfRequired()
    }
    override func viewWillAppear(animated: Bool)
    {
    }
    @IBAction func findFriends(sender: AnyObject) {
        
        if let viewController = storyboard!.instantiateViewControllerWithIdentifier("FriendSearchVC") as? FriendSearchVC {
            self.animator = ZFModalTransitionAnimator(modalViewController: viewController)
            self.animator.dragable = true
            self.animator.bounces = false
            self.animator.behindViewAlpha = 0.5
            self.animator.behindViewScale = 1.0
            self.animator.transitionDuration = 0.7
            self.animator.direction = ZFModalTransitonDirection.Left
            self.animator.setContentScrollView(viewController.tableView)
            
            viewController.transitioningDelegate = self.animator
            viewController.modalPresentationStyle = UIModalPresentationStyle.Custom
            presentViewController(viewController, animated: true, completion: nil)
            
            //navigationController?.pushViewController(viewController, animated: true)
        }
    }
    func loadUserProfile(){
        
        let accessToken = FBSDKAccessToken.currentAccessToken()
        if(accessToken != nil) //should be != nil
        {
        
            let InfoRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name,email,friends,cover"])
            InfoRequest.startWithCompletionHandler({
                (connection, result, error: NSError!) -> Void in
                if error == nil {
                    let json = JSON(result!)
        
                    println(json["name"])
                    println(json["email"])
                    println(json["id"])
                    //println(json["friends"])
                    //println(json["hometown"])
                    //println(json)
                    self.userName.text = json["name"].string!
                    
//                    var pictureUrlString: String = json["cover"]["source"].string! ?? "https://myspace.com/common/images/user.png"
//                    var pictureURL = NSURL(string: pictureUrlString)
                    //self.backgroundImageView.hnk_setImageFromURL(pictureURL!)
                    //self.userCity.text = json["location"].string!
                    
                } else {
                    //println("\(error)")
                }
            })
            
            
            let pictureRequest = FBSDKGraphRequest(graphPath: "me/picture?type=large&redirect=false", parameters: nil)
            //me/picture?width=1080&height=1080&redirect=false For bigger picture size
            pictureRequest.startWithCompletionHandler({
                (connection, result, error: NSError!) -> Void in
                if error == nil {
                    let json = JSON(result!)
                    //println(json)
                    //println(json["data"]["url"])
                    var pictureUrlString: String = json["data"]["url"].string!
                    var pictureURL = NSURL(string: pictureUrlString)
                    self.profileImageView.hnk_setImageFromURL(pictureURL!)
                    
                } else {
                    //println("\(error)")
                }
            })
            
            }
    }
    
    func setSegmentControl(){
        
        segmentControl.items = ["My Fragments", "My Stories"]
        segmentControl.font = UIFont(name: "Avenir-Black", size: 12)
        segmentControl.borderColor = UIColor(white: 1.0, alpha: 0.3)
        segmentControl.selectedIndex = 0
        segmentControl.addTarget(self, action: "segmentValueChanged:", forControlEvents: .ValueChanged)
    }

    func segmentValueChanged(sender: AnyObject?){

        if segmentControl.selectedIndex == 0 {
            
        }else if segmentControl.selectedIndex == 1{
            
        }
    }
    
    
    // MARK: - Navigation
    @IBAction func backButton(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        //self.presentingViewController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        //navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ProfileFragmentSegue" {
            
            let viewController = segue.destinationViewController as? FragmentsVC
            self.fragmentsVC = viewController
            self.fragmentsVC!.filter = "Me"
        }
    }

}

extension ProfileVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("StoryCell", forIndexPath: indexPath) as! StoryCell
        
        if (stories?[indexPath.row].content != nil) {
            cell.contentLabel?.text = stories?[indexPath.row].content
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories?.count ?? 0
    }
    
}

extension ProfileVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    // 3
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    // 4
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell:StoryCell = (cell as? StoryCell)!
        cell.contentLabel.shine()
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell:StoryCell = (cell as? StoryCell)!
    }
    
}
