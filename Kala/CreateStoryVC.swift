//
//  CreateStoryVC.swift
//  Kala
//
//  Created by Professional on 2015-07-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import KTSecretTextView
import Foundation
import ZFDragableModalTransition
import Parse
import BubbleTransition

let customDismissTransitionAC = CreateFragmentTransition()
class CreateStoryVC: UIViewController {

    //var alertView: UIView!
    @IBOutlet weak var textView: PlaceholderTextView!
    @IBOutlet weak var imageview:UIImageView?
    var transitions: [Transition]!
    var secretVC: KTSecretViewController?
    var transitionWords: String!
    var animator:ZFModalTransitionAnimator!
    let transition = BubbleTransition()
    
    var maxLength = 140
    var Xpos = 0
    var Ypos = 0
    var label:UILabel?
    var descriptionLabel:UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func createFragment(sender: CustomButton) {
        
        self.tabBarController?.selectedIndex = 0
    }
    @IBAction func cancelCreation(sender: CustomButton) {
        
        //self.tabBarController?.selectedIndex = 0

    }
    
    override func viewWillAppear(animated: Bool)
    {
        //createView()
        if (self.secretVC == nil) {
            //self.tabBarController?.tabBar.hidden = true
           self.secretVC = KTSecretViewController()
            //var navigationController = UINavigationController(rootViewController: secretVC!)
            self.secretVC!.textView.delegate = self
            self.secretVC?.closeDoneDelegate = self
            self.setInitialTransition()
            //self.secretVC!.transitioningDelegate = self
            //self.secretVC!.modalPresentationStyle = .Custom
            self.presentViewController(self.secretVC!, animated: true, completion: {
                _ in
                
                //Add Char limit label
                self.label = UILabel(frame: CGRectMake(0, 200, 200, 30))
                if let label = self.label{
                    
                    label.font = label.font.fontWithSize(13)
                    label.textColor = UIColor.whiteColor()
                    label.center = self.view.center
                    label.textAlignment = NSTextAlignment.Center
                    label.text = "0"
                    self.secretVC!.view.addSubview(label)
                    
                    
                    label.setTranslatesAutoresizingMaskIntoConstraints(false)
                    let labelHorizontalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.secretVC!.view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 135)
                    self.secretVC!.view.addConstraint(labelHorizontalConstraint)
                    
                    let labelVerticalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.secretVC!.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -260)
                    self.secretVC!.view.addConstraint(labelVerticalConstraint)
                    
                    let labelWidthConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
                    self.secretVC!.view.addConstraint(labelWidthConstraint)
                    
                    let labelHeightConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
                    self.secretVC!.view.addConstraint(labelHeightConstraint)
                }
                
                
                self.descriptionLabel = UILabel(frame: CGRectMake(0, 330, 200, 20))
                
                if let descriptionLabel = self.descriptionLabel {
                    
                    descriptionLabel.font = descriptionLabel.font.fontWithSize(13)
                    descriptionLabel.textColor = UIColor.whiteColor()
                    descriptionLabel.center = self.view.center
                    descriptionLabel.textAlignment = NSTextAlignment.Center
                    descriptionLabel.text = "/\(self.maxLength)"
                    self.secretVC!.view.addSubview(descriptionLabel)
                    
                    descriptionLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
                    let descriptionHorizontalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.secretVC!.view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 160)
                    self.secretVC!.view.addConstraint(descriptionHorizontalConstraint)
                    
                    let descriptionVerticalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.secretVC!.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -260)
                    self.secretVC!.view.addConstraint(descriptionVerticalConstraint)
                    
                    let descriptionWidthConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
                    self.secretVC!.view.addConstraint(descriptionWidthConstraint)
                    
                    let descriptionHeightConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 40)
                    self.secretVC!.view.addConstraint(descriptionHeightConstraint)
                }
                
                self.secretVC!.textView.becomeFirstResponder()
            })
            
        }else{
            self.secretVC = nil
            self.tabBarController?.selectedIndex = 0
        }
        

    }

    //Query Transition that can begin a story
    func setInitialTransition(){
        
        transitionWords = "Once upon two time "
        let transitionQuery = Transition.query()
        transitionQuery!.whereKey("canBegin", equalTo: NSNumber(bool: true))
        transitionQuery!.findObjectsInBackgroundWithBlock {(result: [AnyObject]?, error: NSError?) -> Void in
            
            self.transitions = result as? [Transition] ?? []
            self.transitionWords = self.transitions[0].content
            self.secretVC!.textView.text = self.transitionWords
        }
    }
    
    func createFirstFragment(colorName:String,textureName: String,shareToWorld:Bool) {
        
        let fragment = Fragment()
        fragment.user = PFUser.currentUser()
        fragment.content = self.secretVC!.textView.text /*+ "\n and everyday "*/
        fragment.order = 0
        fragment.story = createStory()
        fragment.colorName = colorName
        fragment.textureName = textureName
        fragment.setValue(NSNumber(bool: false), forKey: "hidden")
        fragment.setValue(NSNumber(bool: shareToWorld), forKey: "shareToWorld")
        fragment.uploadFragment()
    }
    
    func createStory() -> Story{
        var story = Story()
        //story.content = content
        story.uploadStory()
        
        return story
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateStoryVC: UITextViewDelegate {
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        let newLength = count(textView.text.utf16) - transitionWords.length + count(text.utf16) - range.length
        //change the value of the label
        
        if let label = self.label {
            
            label.text?.removeAll(keepCapacity: false)
            label.text? =  String(newLength)
        }
        
        
        if range.location < transitionWords.length{
            return false
        }
        if textView.text.length + ((text.length - transitionWords.length)  - range.length) >= maxLength {
            return false
        }
    
        return true
    }
}
//Mark - Transitions animation
extension CreateStoryVC: UIViewControllerTransitioningDelegate {
    
    /*func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentTransitionAC
    }*/
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customDismissTransitionAC
    }
    
}

extension CreateStoryVC: CloseDoneBtnDelegate {
    
//    func doneBtnSelected(){
//        
//        println("Done Selected")
//        createFirstFragment()
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
    func doneBtnSelected(backgroundColor: String!, _ textureName: String!) {
        
        self.secretVC!.textView.resignFirstResponder()
        println("Done Create Selected \(backgroundColor) ")
        var shareToWorld = true
        let alertController = UIAlertController(title: "Share", message: "Who do you want to share to?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Friend", style: .Default, handler: { action in
            println("Click of default button")
            shareToWorld = false
            self.createFirstFragment(backgroundColor,textureName: textureName,shareToWorld: shareToWorld)
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "World", style: .Cancel, handler: { action in
            println("Click of cancel button")
            self.createFirstFragment(backgroundColor,textureName: textureName,shareToWorld: shareToWorld)
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.secretVC!.presentViewController(alertController, animated: true, completion: nil)
        
    }

}

//extension FragmentsVC:UIViewControllerTransitioningDelegate{
//    // MARK: UIViewControllerTransitioningDelegate
//    
//    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .Present
//        transition.startingPoint = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 100)
//        transition.bubbleColor = defaultColor
//        return transition
//    }
//    
//    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .Dismiss
//        transition.startingPoint = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 100)
//        transition.bubbleColor = defaultColor
//        return transition
//    }
//}