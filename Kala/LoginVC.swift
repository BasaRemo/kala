//
//  LoginVC.swift
//  Kala
//
//  Created by Professional on 2015-08-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import FBSDKLoginKit
import ParseUI
import Spring
import SABlurImageView

let backgroundAppColor = UIColor(hex: "95B174")
class LoginVC: PFLogInViewController {

    let permissions = ["public_profile", "email", "user_friends"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onProfileUpdated:", name:FBSDKProfileDidChangeNotification, object: nil)
        
        self.view.backgroundColor = UIColor.darkGrayColor()
        self.logInView!.logo = nil
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Change Facebook Button Position
        self.logInView!.facebookButton?.frame = CGRectMake(self.logInView!.facebookButton!.frame.origin.x
            , 380, self.logInView!.facebookButton!.frame.width , self.logInView!.facebookButton!.frame.height)
        self.logInView!.facebookButton?.layer.cornerRadius = 20
        
        self.logInView!.facebookButton?.setTranslatesAutoresizingMaskIntoConstraints(false)
        let facebookButtonHorizontalConstraint = NSLayoutConstraint(item: self.logInView!.facebookButton!, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.logInView!.addConstraint(facebookButtonHorizontalConstraint)
        
        let facebookButtonVerticalConstraint = NSLayoutConstraint(item: self.logInView!.facebookButton!, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 100)
        self.logInView!.addConstraint(facebookButtonVerticalConstraint)
        
        let facebookButtonWidthConstraint = NSLayoutConstraint(item: self.logInView!.facebookButton!, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: self.logInView!.facebookButton!.frame.width)
        self.logInView!.addConstraint(facebookButtonWidthConstraint)
        
        let facebookButtonHeightConstraint = NSLayoutConstraint(item: self.logInView!.facebookButton!, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: self.logInView!.facebookButton!.frame.height)
        self.logInView!.addConstraint(facebookButtonHeightConstraint)
        
        
        let logoView = SpringImageView(image: UIImage(named:"tree.png"))
        logoView.frame = CGRectMake(90, 100, 150, 150)
        logoView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.logInView!.addSubview(logoView)
        let horizontalConstraint = NSLayoutConstraint(item: logoView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.logInView!.addConstraint(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint(item: logoView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -100)
        self.logInView!.addConstraint(verticalConstraint)
        
        let widthConstraint = NSLayoutConstraint(item: logoView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 150)
        self.logInView!.addConstraint(widthConstraint)
        
        let heightConstraint = NSLayoutConstraint(item: logoView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 150)
        self.logInView!.addConstraint(heightConstraint)
        
        logoView.animation = "slideUp"
        logoView.duration = 1.5
        logoView.animate()
        
        
//        let logoView = UIImage(image: UIImage(named:"tree.png"))
//        logoView.frame = CGRectMake(120, 100, 150, 150)
//        
        var label = UILabel(frame: CGRectMake(0, 200, 200, 30))
        label.font = label.font.fontWithSize(30)
        label.textColor = UIColor.whiteColor()
        label.center = self.logInView!.center
        label.textAlignment = NSTextAlignment.Center
        label.text = "Palaver"
        self.logInView!.addSubview(label)
        
        
        label.setTranslatesAutoresizingMaskIntoConstraints(false)
        let labelHorizontalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.logInView!.addConstraint(labelHorizontalConstraint)
        
        let labelVerticalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
        self.logInView!.addConstraint(labelVerticalConstraint)
        
        let labelWidthConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 200)
        self.logInView!.addConstraint(labelWidthConstraint)
        
        let labelHeightConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 30)
        self.logInView!.addConstraint(labelHeightConstraint)
        
        var descriptionLabel = UILabel(frame: CGRectMake(0, 330, 200, 20))
        descriptionLabel.font = descriptionLabel.font.fontWithSize(12)
        descriptionLabel.textColor = UIColor.whiteColor()
        descriptionLabel.center = self.logInView!.center
        descriptionLabel.textAlignment = NSTextAlignment.Center
        descriptionLabel.text = "Collaborative Story Telling"
        self.logInView!.addSubview(descriptionLabel)
        
        descriptionLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        let descriptionHorizontalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.logInView!.addConstraint(descriptionHorizontalConstraint)
        
        let descriptionVerticalConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 25)
        self.logInView!.addConstraint(descriptionVerticalConstraint)
        
        let descriptionWidthConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 200)
        self.logInView!.addConstraint(descriptionWidthConstraint)
        
        let descriptionHeightConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 30)
        self.logInView!.addConstraint(descriptionHeightConstraint)
        
        let backgroundView:SABlurImageView = SABlurImageView(frame: self.view.frame)
        backgroundView.image = UIImage(named:"time.png")
//        backgroundView.backgroundColor = backgroundAppColor
        self.view.insertSubview(backgroundView, belowSubview: self.logInView!.facebookButton!)
        //applyBlurEffect()
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.logInView!.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view.bounds
            self.logInView!.insertSubview(blurEffectView, aboveSubview: backgroundView)
            //self.view.insertSubview(blurEffectView, belowSubview: self.view.viewWithTag(10)!) //if you have more UIViews on screen, use insertSubview:belowSubview: to place it underneath the lowest view instead
            
        } else {
            self.view.backgroundColor = UIColor.blackColor()
        }

    }
    
    
    @IBAction func fbLoginClick(sender: UIButton) {
        
        PFFacebookUtils.logInInBackgroundWithReadPermissions(permissions) {
            (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    println("User signed up and logged in through Facebook!")
                    self.presentAppView()
                } else {
                    println("User logged in through Facebook!")
                    self.presentAppView()
                }
            } else {
                println("Uh oh. The user cancelled the Facebook login.")
            }
        }
        FBSDKProfile.enableUpdatesOnAccessTokenChange(true)

    }
    func presentAppView(){
        let startViewController = storyboard!.instantiateViewControllerWithIdentifier("CustomTabBarController") as! CustomTabBarController
        presentViewControllerFromTopViewController(startViewController, animated: true, completion: nil)
    }
    func onProfileUpdated(notification: NSNotification)
    {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginVC:PFLogInViewControllerDelegate {
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!){
        println("Success Login")
        //PFUser.logInWithUsername("test", password: "test")
        //self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {
        println("Logged In")
    }
    func logInViewControllerDidCancelLogIn(logInController: PFLogInViewController) {
        println("Cancel Log In")
    }
    func logInViewController(logInController: PFLogInViewController, didFailToLogInWithError error: NSError?) {
        println("Fail Log In")
    }

}
