//
//  TextureVC.swift
//  Kala
//
//  Created by Professional on 2015-07-30.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class TextureVC: UIViewController {

    @IBOutlet weak var imageview:UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.9
        imageview?.alpha = 0.5
        imageview?.translatesAutoresizingMaskIntoConstraints()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool)
    {
        //createView()
        //self.view.alpha = 0.9
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
