//
//  CustomTextVC.swift
//  Kala
//
//  Created by Professional on 2015-08-03.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class CustomTextVC: UIViewController {
    

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var textView: PlaceholderTextView!
    let identifier = "collectionCell"
    var defaultCellSize: CGSize {
        get {
            return CGSizeMake(self.view.frame.width, self.view.frame.height)
        }
    }
    var paletteCellSize: CGSize{
        get {
            return CGSizeMake(self.view.frame.width/6, self.view.frame.height)
        }
    }
    
    var defaultLayout: UICollectionViewFlowLayout =  UICollectionViewFlowLayout() {

        didSet {
            // Implement the setter here.
            //self.defaultLayout = UICollectionViewFlowLayout()
            self.defaultLayout.itemSize = self.defaultCellSize
            self.defaultLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.defaultLayout.minimumInteritemSpacing = 0
            self.defaultLayout.minimumLineSpacing = 0
            self.defaultLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        }
    }
    var paletteLayout: UICollectionViewFlowLayout =  UICollectionViewFlowLayout(){

        didSet {
            // Implement the setter here.
            self.paletteLayout = UICollectionViewFlowLayout()
            self.paletteLayout.itemSize = self.paletteCellSize
            self.paletteLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.paletteLayout.minimumInteritemSpacing = 0
            self.paletteLayout.minimumLineSpacing = 0
            self.paletteLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        }
    }
    var longPressGestureRecognizer: UILongPressGestureRecognizer {
        get {
            return self.longPressGestureRecognizer
        }
        set {
            // Implement the setter here.
        }
    }
    
    var colors = [ UIColor.redColor(),  UIColor.blueColor(), UIColor.yellowColor(),UIColor.greenColor(),UIColor.brownColor(),UIColor.purpleColor()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpCollectionView()
    }

    func setUpCollectionView(){
        
        self.defaultLayout = UICollectionViewFlowLayout()
        self.defaultLayout.itemSize = self.paletteCellSize
        self.defaultLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.defaultLayout.minimumInteritemSpacing = 0
        self.defaultLayout.minimumLineSpacing = 0
        self.defaultLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        self.paletteLayout = UICollectionViewFlowLayout()
        self.paletteLayout.itemSize = self.paletteCellSize
        self.paletteLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.paletteLayout.minimumInteritemSpacing = 0
        self.paletteLayout.minimumLineSpacing = 0
        self.paletteLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: paletteLayout)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.pagingEnabled = true
        //self.collectionView.collectionViewLayout = defaultLayout
        self.collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.identifier)
        //self.view.addSubview(self.collectionView)
        self.view.insertSubview(self.collectionView, belowSubview: textView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CustomTextVC : UICollectionViewDataSource {
    
    //Mark - UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return colors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath) as! UICollectionViewCell
        cell.backgroundColor = self.colors[indexPath.row]
        return cell
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return self.defaultCellSize
    }
}

extension CustomTextVC : UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            println("\(indexPath.row)");
    }
}
